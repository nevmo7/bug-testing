package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoSettings;

import library.entities.ILoan.LoanState;
import library.entities.helpers.BookHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	@Mock IBookHelper mockBookHelper;
	@Mock IPatronHelper mockPatronHelper;
	@Mock ILoanHelper mockLoanHelper;
	@Mock IBook mockBook;
	@Mock IPatron mockPatron;
	ILoan loan;

	@BeforeEach
	void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
		java.util.Date dueDate = Calendar.getInstance().getDueDate(0);
		loan = new Loan(mockBook, mockPatron, dueDate, LoanState.OVER_DUE, 1);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFineForLoanOverDueBy1Day() {
		Library library = new Library(mockBookHelper, mockPatronHelper, mockLoanHelper);
		Calendar.getInstance().incrementDate(1);
		double actual = library.calculateOverDueFine(loan);
		double expected = 1;
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testCalculateOverDueFineForBug2() {
		Library library = new Library(mockBookHelper, mockPatronHelper, mockLoanHelper);
		Calendar.getInstance().incrementDate(4);
		double actual = library.calculateOverDueFine(loan);
		double expected = 4;
		
		assertEquals(expected, actual);
	}

}
